package TestAutomationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class AssessmentTask2_2 {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
				// Create a new instance of the Firefox driver
				WebDriver driver = new FirefoxDriver();
				driver.manage().window().maximize();
				
				// Storing the Application Url in the String variable
				String url = "http://link.springer.com/";
		 
				//Launch the WebSite
				driver.get(url);
		 
				// Storing Title name in the String variable
				String title = driver.getTitle();
		 
				// Printing Title in the Console window
				System.out.println("Title of the page is : " + title);
		 
				// Storing URL in String variable and check for correctness
				String actualUrl = driver.getCurrentUrl();
		 
				if (actualUrl.equals(url)){
					System.out.println("Verification Successful - The correct Url is opened.");
				}else{
					System.out.println("Verification Failed - An incorrect Url is opened.");
					//In case of Fail, print the actual and expected URL for the record purpose
					System.out.println("Actual URL is : " + actualUrl);
					System.out.println("Expected URL is : " + url);
				}
		 
				//Enter search string, search and check results
				driver.findElement(By.name("query")).sendKeys("Astronomy");
				driver.findElement(By.id("search")).click();
				//Storing number of results in String and print 
				WebElement element = driver.findElement(By.className("header"));
				String actualResult = element.getText();
				System.out.println("Search Result is : " + actualResult);
				//Storing number of results and check if they are equal
				String result = driver.findElement(By.className("number-of-search-results-and-search-terms")).getText();
				if (actualResult.equals(result)){
					System.out.println("Verification Successful - The correct number of Search Results are shown.");
				}else{
					System.out.println("Verification Failed - An incorrect number of Search Results is shown.");
					//In case of Fail, print the actual and expected number for the record purpose
					System.out.println("Actual URL is : " + result);
					System.out.println("Expected URL is : " + actualResult);
				}
				
				//Closing browser
				driver.close();
	}

}
